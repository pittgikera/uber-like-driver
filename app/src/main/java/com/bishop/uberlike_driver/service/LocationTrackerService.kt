package com.bishop.uberlike_driver.service

import com.bishop.uberlike_driver.common.DRIVERS_COLLECTION
import com.bishop.uberlike_driver.model.Driver
import com.bishop.uberlike_driver.model.GeoLocation
import com.google.android.gms.location.LocationResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.sprotte.geolocator.tracking.service.LocationTrackerUpdateIntentService
import timber.log.Timber

class LocationTrackerService : LocationTrackerUpdateIntentService() {
    private val db = FirebaseFirestore.getInstance()
    private val user = FirebaseAuth.getInstance().currentUser

    override fun onLocationResult(locationResult: LocationResult) {
        Timber.v("onLocationResult $locationResult")

        updateUserLocation(locationResult)
    }

    private fun updateUserLocation(locationResult: LocationResult) {
        user?.let {
            val loc = locationResult.lastLocation

            val currentLocation = Driver().apply {
                name = user.displayName
                location = GeoLocation("Point", arrayListOf(loc.latitude, loc.longitude))
            }

            db.collection(DRIVERS_COLLECTION).document(user.uid)
                .set(currentLocation)
                .addOnSuccessListener { Timber.d("DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Timber.e("Error writing document $e") }
        }
    }
}