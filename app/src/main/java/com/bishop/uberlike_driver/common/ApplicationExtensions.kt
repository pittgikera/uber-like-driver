package com.bishop.uberlike_driver.common

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import kotlin.reflect.KClass

fun <T : Activity> KClass<T>.start(
    activity: Activity,
    finish: Boolean = false,
    extraName: String = "",
    extraValue: Any = ""
) {
    Intent(activity, this.java).apply {
        if (extraName.isNotEmpty()) {
            when (extraValue) {
                is String -> {
                    putExtra(extraName, extraValue)
                }
                is Int -> {
                    putExtra(extraName, extraValue)
                }
                is Boolean -> {
                    putExtra(extraName, extraValue)
                }
                is Float -> {
                    putExtra(extraName, extraValue)
                }
                is Long -> {
                    putExtra(extraName, extraValue)
                }
                else -> throw UnsupportedOperationException("Not yet implemented")
            }

        }
        activity.startActivity(this)
    }
    if (finish) {
        activity.finish()
    }
}


fun checkInternet(context: Context) {
    if (!context.isConnectedToInternet()) {
        context.toastyError("No Internet Connection")
    }
}

fun Context.isConnectedToInternet(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    val activeNetwork = cm.activeNetworkInfo

    return activeNetwork != null && activeNetwork.isConnected
}
