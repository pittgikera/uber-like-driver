package com.bishop.uberlike_driver.common

import android.view.View

inline fun View.onClick(crossinline onClickHandler: () -> Unit) {
    setOnClickListener { onClickHandler() }
}

fun View.hideView(){
    this.visibility = View.INVISIBLE
}

fun View.showView(){
    this.visibility = View.VISIBLE
}

fun View.hideGoneView(){
    this.visibility = View.GONE
}
