package com.bishop.uberlike_driver.ui.main

import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener

class LocationPermissionListener(private val activity: MainActivity) : PermissionListener {

    override fun onPermissionGranted(response: PermissionGrantedResponse) {
        activity.permissionGranted(response.permissionName)
    }

    override fun onPermissionRationaleShouldBeShown(
        permission: PermissionRequest,
        token: PermissionToken
    ) {
        activity.permissionRationale(token)
    }

    override fun onPermissionDenied(response: PermissionDeniedResponse) {
        activity.permissionDenied(response.permissionName, response.isPermanentlyDenied)
    }

}