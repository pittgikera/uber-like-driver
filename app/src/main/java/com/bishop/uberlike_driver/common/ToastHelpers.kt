package com.bishop.uberlike_driver.common

import android.app.Activity
import android.content.Context
import android.view.Gravity
import android.widget.Toast
import androidx.fragment.app.Fragment
import es.dmoral.toasty.Toasty


fun Activity.toastyWarn(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.warning(this, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }

}

fun Activity.toastyError(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.error(this, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Activity.toastyInfo(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.info(this, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Activity.toastySuccess(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.success(this, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Context.toastySuccess(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.success(this, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Activity.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.normal(this, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}


fun Fragment.toastyWarn(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.warning(this.context as Context, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }

}

fun Fragment.toastyError(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.error(this.context as Context, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Context.toastyError(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.error(this, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Fragment.toastyInfo(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.info(this.context as Context, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Fragment.toastySuccess(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.success(this.context as Context, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}

fun Fragment.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toasty.normal(this.context as Context, message, duration).apply {
        setGravity(Gravity.CENTER, 0, 0)
        show()
    }
}