package com.bishop.uberlike_driver.ui.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bishop.uberlike_driver.common.start
import com.bishop.uberlike_driver.ui.main.MainActivity
import com.bishop.uberlike_driver.ui.welcome.WelcomeActivity
import com.google.firebase.auth.FirebaseAuth

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        checkIfAuthenticated()
    }

    private fun checkIfAuthenticated() {
        val user = FirebaseAuth.getInstance().currentUser

        if (user != null) {
            MainActivity::class.start(this, true)
        } else {
            WelcomeActivity::class.start(this, true)
        }
    }
}
