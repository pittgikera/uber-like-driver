package com.bishop.uberlike_driver.ui.main

import android.Manifest.permission
import android.location.Location
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Observer
import com.bishop.uberlike_driver.App
import com.bishop.uberlike_driver.R
import com.bishop.uberlike_driver.common.DRIVERS_COLLECTION
import com.bishop.uberlike_driver.common.onClick
import com.bishop.uberlike_driver.common.start
import com.bishop.uberlike_driver.data.PreferenceRepository
import com.bishop.uberlike_driver.model.Driver
import com.bishop.uberlike_driver.model.GeoLocation
import com.bishop.uberlike_driver.service.LocationTrackerService
import com.bishop.uberlike_driver.ui.splash.SplashActivity
import com.firebase.ui.auth.AuthUI
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener
import com.sprotte.geolocator.tracking.LocationTracker
import io.nlopez.smartlocation.SmartLocation
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import timber.log.Timber


class MainActivity : AppCompatActivity() {
    private lateinit var preferenceRepository: PreferenceRepository
    private lateinit var firebaseUser: FirebaseUser
    private val db = FirebaseFirestore.getInstance()

    private var mIsOnline = false

    private lateinit var locationPermissionListener: CompositePermissionListener

    private lateinit var rootLayout: CoordinatorLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        preferenceRepository = (application as App).preferenceRepository
        rootLayout = findViewById(R.id.cl_main_root)

        FirebaseAuth.getInstance().currentUser?.let {
            firebaseUser = it
        }

        checkOnlineStatus()
        createPermissionListeners()
        initUi()
    }

    private fun initUi() {
        preferenceRepository.nightModeLive.observe(this, Observer { nightMode ->
            nightMode?.let { delegate.localNightMode = it }
        }
        )

        if (::firebaseUser.isInitialized) {
            tv_main_salutations?.text =
                getString(R.string.home_salutations, firebaseUser.displayName)
        }

        btn_main_toggleOnlineStatus.onClick {
            toggleOnlineStatus()
        }
    }

    private fun checkOnlineStatus(){
        btn_main_toggleOnlineStatus?.isEnabled = false

        db.collection(DRIVERS_COLLECTION).document(firebaseUser.uid)
            .get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    Timber.d( "DocumentSnapshot data: ${document.data}")
                    changeOnlineStatus(true)
                } else {
                    Timber.d("No such document")
                    changeOnlineStatus(false)
                }
            }
            .addOnFailureListener { exception ->
                Timber.d("get failed with $exception")
                tv_main_onlineStatus?.text = exception.localizedMessage
            }
    }

    private fun isDriverOnline(isOnline: Boolean){
        mIsOnline = isOnline
        btn_main_toggleOnlineStatus?.isEnabled = true
        when(isOnline){
            true->{
                tv_main_onlineStatus?.text = getText(R.string.online)
                btn_main_toggleOnlineStatus?.text = getText(R.string.go_offline)
            }

            false->{
                tv_main_onlineStatus?.text = getText(R.string.offline)
                btn_main_toggleOnlineStatus?.text = getText(R.string.go_online)
            }
        }
    }

    private fun toggleOnlineStatus() {
        Dexter.withActivity(this)
            .withPermission(permission.ACCESS_FINE_LOCATION)
            .withListener(locationPermissionListener)
            .onSameThread()
            .check()
    }

    private fun changeOnlineStatus(isOnline:Boolean){
        isDriverOnline(isOnline)

        when(isOnline){
            true->{

                LocationTracker.requestLocationUpdates(this, LocationTrackerService::class.java)
            }

            false->{
                LocationTracker.removeLocationUpdates(this)
                db.collection(DRIVERS_COLLECTION).document(firebaseUser.uid)
                    .delete()
                    .addOnSuccessListener { Timber.d( "DocumentSnapshot successfully deleted!") }
                    .addOnFailureListener { e -> Timber.e( "Error deleting document $e") }


            }
        }
    }

    private fun updateUserLocation(loc: Location) {
        Timber.e("DRIVER LAT=> ${loc.latitude}")
        firebaseUser.let {
            //val loc = locationResult.lastLocation

            val currentLocation = Driver().apply {
                name = firebaseUser.displayName
                location = GeoLocation("Point", arrayListOf(loc.latitude, loc.longitude))
            }

            db.collection(DRIVERS_COLLECTION).document(firebaseUser.uid)
                .set(currentLocation)
                .addOnSuccessListener { Timber.d("DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Timber.e("Error writing document $e") }
        }
    }

    private fun createPermissionListeners() {
        locationPermissionListener = CompositePermissionListener(
            LocationPermissionListener(this),
            SnackbarOnDeniedPermissionListener.Builder
                .with(rootLayout, R.string.location_permission_denied_feedback)
                .withOpenSettingsButton("Settings")
                .withCallback(object : Snackbar.Callback() {
                    override fun onShown(snackbar: Snackbar?) {
                    }

                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                    }
                }).build()
        )
    }

    fun permissionGranted(permission: String) {
        Timber.e("Location granted")
        changeOnlineStatus(!mIsOnline)
    }

    fun permissionDenied(permission: String, isPermanentlyDenied: Boolean) {
        Timber.e("Location denied")
    }

    fun permissionRationale(token: PermissionToken) {
        AlertDialog.Builder(this).setTitle(R.string.permission_rationale_title)
            .setMessage(R.string.permission_rationale_message)
            .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.dismiss()
                token.cancelPermissionRequest()
            }
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
                token.continuePermissionRequest()
            }
            .setOnDismissListener { token.cancelPermissionRequest() }
            .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.action_sign_out -> {
                AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener {
                        // ...
                        SplashActivity::class.start(this, true)
                    }
                true
            }
            R.id.action_switch_theme -> {
                preferenceRepository.isDarkTheme = !preferenceRepository.isDarkTheme
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
